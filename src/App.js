import React from 'react';
import RecipeList from './components/RecipeList';

import './App.css';

function App() {

  return (
    <div>
      <RecipeList list={RecipeList}/>
    </div>
  );
}

export default App;