import React from 'react';
import './styles.scss';

const Recipe = ({title, image, ingredients}) => {
  return(
    <div className='Recipe'>
      <h3>{title}</h3>
      <img src={image} alt='' />
      <ol>
        {ingredients.map(ingredient => (
          <li>{ingredient.text}</li>
        ))}
      </ol>
    </div>
  )
};

export default Recipe;

