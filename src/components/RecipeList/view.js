import React, { useEffect, useState } from 'react';
import Recipe from '../Recipe/view';
import './styles.scss';

function RecipeList() {

  const APP_ID = 'f7b12dbe';
  const APP_KEY = 'e3cf5e019833b138bf7ef07aed12ffef';

  const [recipes, setRecipes] = useState([]);
  const [search, setSearch] = useState('');
  const [query, setQuery] = useState('chicken');


  useEffect(() => {
    getRecipes();
  }, [query]);

  const getRecipes = async () => {
    const response = await fetch(
      `https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`
      );
    const data = await response.json();
    setRecipes(data.hits);
    console.log(data);
  };

  const updateSearch = e => {
    setSearch(e.target.value);
  };

  const getSearch = e => {
    e.preventDefault();
    setQuery(search);
    setSearch('');
  }

  return (
    <div className="RecipeList">
      <form onSubmit={getSearch} className='search-form'>
        <input 
        className='search-bar' 
        type='text' 
        value={search} 
        onChange={updateSearch}/>
          <button className='search-button' type='submit'>
            Search
          </button>
      </form>
      {recipes.map(recipe => (
        <Recipe
          key={recipe.recipe.label} 
          title={recipe.recipe.label} 
          image={recipe.recipe.image}
          ingredients={recipe.recipe.ingredients}
        />
      ))};
    </div>
  );
}


export default RecipeList;
